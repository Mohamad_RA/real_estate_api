<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $images = [
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQe4BUGwigipTB0maLc2Jq37oiRGLk92uNsFQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTO4Niy7IYNNG7szVAXVaRx9N7Z67hIvud8SA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQlxMd5wJtGdvpIvHbfMYCwdceUDBohaPpSw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT1rAxZylfKEAe8c1zemBnH_VscBo5Cryh4Pw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThIHlk4XhaacHQJSeOGie92vR7AOcJJ0DI_w&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThIHlk4XhaacHQJSeOGie92vR7AOcJJ0DI_w&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQGOTRnBQ9UTuTgPpl_95gy0teeUxDQ_KSl-Q&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTzYP68DCokRFd5DNZrklPm3mHLamRew1UzFA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzInLVH8V4voO0V-zZlLZS954ss4_o7j5Dhw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSAJmgs4DA9TFZKfH3AVDjC3e919CIT3ZxfAw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7NitNJtQ3CjsqtZ10G7TqlL6XWEsQr2klHw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRe7paYWtQ4TATgoOnxOzqJRbTzkIcTCjeAXg&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRCX9_SGYiIFyWZ7zU7LRx_NcSpK-Yyo7j_Vw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ0jeNqLeCDnTsSujY8oil8eLf7-J88GBeKow&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdpIxqNFLCvZRejM3aNQz_7GzaRu-PFMhI2A&usqp=CAU',
            'https://img.freepik.com/free-photo/portrait-young-beautiful-playful-woman-with-bun-posing_176420-12392.jpg?size=626&ext=jpg&ga=GA1.2.699125266.1619481600',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTrYnLzz28jbd5ZWdQuyTGzt6kWXl4frSdEl5ylF8wKWzCkehxyu0CB0u1Jrg9PhMOtX7k&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVNIaW8deJr7qk5QkCdPcIdR0kaVITxNBxZQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIH3GGFBgfpQcRPwP69eLLFoB0NlHSF5VMIw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQKc5oY5RKDYmPbeKXK_rhzmyi156PkTHHfPw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQegx_DPIU_EI3eSiWFGcLXFVmxF0h23Wv3VQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSPKRlfSPpaJQO7Ri9yRUZRA0ATncqwA2T8dw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRWHjMzM5qzBjS64SJEIyCQkLgXODzDLznFPOt54tmyPNXP3BQ78_AN83FAlbeGujmuPCg&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTJkueqf2PfkXtUe_p_kd7j4SBwBEtacRFDmB7O5situL9zj1QSbuV9QWOx8wDqqEDPf5g&usqp=CAU',
            'https://images.unsplash.com/photo-1604426633861-11b2faead63c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1000&q=80',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRAGd77DrnPCjI9Mig2uVdlQHpVisVRAIg0MQ&usqp=CAU',
            'http://images.unsplash.com/photo-1438761681033-6461ffad8d80?crop=entropy&cs=tinysrgb&fit=max&fm=jpg&ixid=MnwxMjA3fDB8MXxhbGx8fHx8fHx8fHwxNjI0NTM5ODMz&ixlib=rb-1.2.1&q=80&w=1080',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJw9hyWznRlyhE4h-rNZQ7AmlK9C1Ol2OG3w&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSJjLIedkNw_6jXPFyh44DfkERo-vsHAGepLA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR7t5JaaMWEpecudVacC-EKDoTZfSSgLY1nDg&usqp=CAU',
        ];


        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'image' =>  $images[rand(0, (count($images) - 1))],

            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
