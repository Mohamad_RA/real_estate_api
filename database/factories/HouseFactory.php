<?php

namespace Database\Factories;

use App\Models\House;
use Illuminate\Database\Eloquent\Factories\Factory;

class HouseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = House::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {


        $other_images = [
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTp6FRCKQOj76w0r5NuI4D-ypGvtYyBI54wiA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEge5cgM1hmheZc1HYb1MdNLhnn6UipVg5N5qFx8f0s-DMVDtED9DSdKT5BZByQZpTHHg&usqp=CAU',
            'https://a0.muscache.com/im/pictures/688e7abe-7d04-459b-ac32-f9a6b4379bb0.jpg?im_w=720',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ04VkIjoNitAtCKKHk3iqQWCpHXooD8fp-V6kzk8QZJSyb-FghgQXghWCcHbiRj9mdnKE&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTkqwpSViG_QQM93Lr1IkE-QuiF6Chl415BCA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRmGGSPZ2uJ0EDMyQXTe0V_tp2VPOYn7kuNvw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQwZyRqfv_TqFhdMzao4fhdtv6SfDkViHS7SA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSdTNNdXolTqU5S_unkEXy8w5qNuBuqfooNvA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRTuu7JsC5vleKDBllXaEMYJphaRZtXwqFliQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQnmwQmjq0YCuu3JvkAfQo9lCxm-yD835CV4g&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3kBDif1xHQYP41v4HoFBWTJwRqp3JJXjzzQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTbkMbnQ-r8u3xCgS-D_bahHVWI8qSjt5seHw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ12CltOjsEOOGgSIZe4YfSHpUrnJS1cg2jAA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTcxwYx__hKSSUxPKjxIGx_yBY4-grXpmEbgQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLzoKQkoMG6Kp4Fz7mpUHUgA4cNCF2-nKIqg&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQxppzLY3woM9p_mOYf39QOiW_YYKsr4avLAA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR78f2zELddQgF64c1HmMfjSZ5cWv1cf98g3w&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTZWzemwRCLqieoIijc59nU2EcCF549OT_LEg&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYvfO3FyrJX2x-T1dlDtnuXShsFo3DKbBrhQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQLz7C1agt5K9AkLDC9LoiU42zvwO5Xs8NpFg&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ8ogFFesVZ3OsNeCrHsHgVIyOZKvUmkjeqpQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRe4Ne4E-xKJrIDdn0Mrh4TKO03d0CfW0mjIw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTEuxqpYaXs8sviTVsYVLXMVdDkQNLon3t-ZQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ1bbmFYnC79zr-ArmHUYKhcRZaUBOarGxYWQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQpe31da-VeToCZpBsDc9ee3xu7ZpJlsmA9Q&usqp=CAU',
        ];

        for ($i = 0; $i < 5; $i++) {
            $other_image[$i] = $other_images[rand(0, (count($other_images) - 1))];
        };


        $images = [
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT7gP3htLQPb__H8vwKFuVunSxAPwLXlY0XZw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRIgfJ6tUMjBM0SEbYcTgQNONofjTFTT4OgVQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSR_E-u4_VXT1t1E0BdpgvaPjhyEckgwJnhgg&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToZYPWJ4heTILUTMnU0vPLhFTSBOYkFmKy-g&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDL3t7pslPUNlOMctlMKTA9Doqb-CfPQsD4w&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTmABkgMh7GY256ZrRm3BOGn8SR2TmEOWTsw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS2sym9p2inlBNMrACjiFRCodsTOqAHPMfjEw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcShwF4ouCiNouOPd5fdEG6vnb_wKxdWNs6yVQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSe7gmR21T8iBj2odPpz7pbQx-AzDxakTBsaQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTmsntjSN9bKar8CWMJRF8oRsi7ZXFA_8xy8g&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRM0mJllJ0m2fVzdYLscTFlibH1W4slBsMHcg&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQbboyyPkX1sRKo4izZZ_bB4kiqtYEgVfIpqg&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQXLx9DFjfN4SzLmS14XwqKO2DPFvB088HXEA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRpAF5AFlxVxv1WBScQSCv3bNxBnB2dHqtvlg&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTOV3ZR3M2CnY-GOFhtRqF9p8fIfnxGuXFwLw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS20YaEF7bE1I8Sx5FeD9e-0dmPNVqdk2oxgA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVPtoe71MIOIxEfGXAqNv061q_gyLhV0L_4A&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTAZJ0YPyKledrBcLCpFXKvmT1MIKPdv5d7Xw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRBI5jhiEP-Tb-3Xdx8dL7B7UlwSHMjUFK3Sw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS3ZKKs0jNHCxELrP486aSv0OnFf7tXgv2QfA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSTmABkgMh7GY256ZrRm3BOGn8SR2TmEOWTsw&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIcgudP6kofsPSWeRtgy0AUwsBs4XKCh-p2g&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRQ70Dj1MiCyJKfXlE6jNW1PeCScd8zkOrdqQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRZwqph6GlnNF2IIIICR2YqRabFkgrvkl182w&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTsA8n_f1CB9Zv19JH_RV3R8KryEOt7ukoACA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSd58Wxw0IpPDJPjK6IqkWzk9etYNqw1OfI0A&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTa7_3j3yxF1A4pGCMkESIqUR9X3ImkEUBhtg&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTc8HU4C8RxM9bESLaKODY0NoyVXc3UTqpCZQ&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_h_gumV3A38SHGVVWQFYMRjHv5y69WlVQHA&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTFNRW4lNqb90avws9OTsdZwfNElf26Xd_23A&usqp=CAU',
            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTIDJEZRG-CxchyPJGktyPfS3rOmOKU6zs6lg&usqp=CAU',
        ];

        return [
            'description' => $this->faker->text($maxNbChars = 200),
            'street_address' => $this->faker->address,
            'other_image' => json_encode($other_image),
            'base_image' => $images[rand(0, (count($images) - 1))],
            'price' => $this->faker->randomFloat(NULL, 5000, 100000),
            'bedrooms' => $this->faker->numberBetween(1, 10),
            'bathrooms' => $this->faker->numberBetween(1, 10),
            'sqft_living' => $this->faker->numberBetween(1, 10),
            'sqft_lot' => $this->faker->numberBetween(1, 10),
            'floors' => $this->faker->numberBetween(1, 10),
            'waterfront' => $this->faker->numberBetween(1, 10),
            'view' => $this->faker->numberBetween(1, 10),
            'condition' => $this->faker->numberBetween(1, 10),
            'grade' => $this->faker->numberBetween(1, 10),
            'sqft_above' => $this->faker->numberBetween(1, 10),
            'sqft_basement' => $this->faker->numberBetween(1, 10),
            'yr_built' => $this->faker->numberBetween(1, 10),
            'yr_renovated' => $this->faker->numberBetween(1, 10),
            'zipcode' => $this->faker->numberBetween(1, 10),
            'lattitude' => $this->faker->latitude($min = -90, $max = 90),
            'longitude' => $this->faker->longitude($min = -180, $max = 180),
            'region_id' =>  \App\Models\Region::all()->random()->id,
            'user_id' =>  \App\Models\User::all()->random()->id
        ];
    }
}
