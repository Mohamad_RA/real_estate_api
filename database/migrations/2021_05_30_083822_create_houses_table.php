<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {  
        Schema::create('houses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description')->nullable();
            $table->string('street_address')->nullable();
            $table->longText('base_image')->nullable();
            $table->longText('other_image')->nullable();
            $table->double('price')->nullable();

            $table->Integer('bedrooms')->nullable();
            $table->Integer('bathrooms')->nullable();
            $table->Integer('sqft_living')->nullable();
            $table->Integer('sqft_lot')->nullable();
            $table->Integer('floors')->nullable();
            $table->Integer('waterfront')->nullable();
            $table->Integer('view')->nullable();
            $table->Integer('condition')->nullable();
            $table->Integer('grade')->nullable();
            $table->Integer('sqft_above')->nullable();
            $table->Integer('sqft_basement')->nullable();
            $table->Integer('yr_built')->nullable();
            $table->Integer('yr_renovated')->nullable();
            $table->Integer('zipcode')->nullable();
            $table->string('lattitude')->nullable();
            $table->string('longitude')->nullable();

            $table->foreignId('region_id');
            $table->foreignId('user_id')->nullable();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('region_id')->references('id')->on('regions')->onDelete('cascade');

            $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
