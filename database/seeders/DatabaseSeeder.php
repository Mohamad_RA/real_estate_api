<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Region::factory(10)->create();
        \App\Models\User::factory(20)->create();
        \App\Models\House::factory(100)->create();
        \App\Models\Owner::factory(80)->create();
        \App\Models\Comment::factory(300)->create();
        \App\Models\Reply::factory(400)->create();
        \App\Models\HouseOwner::factory(100)->create();
        \App\Models\Like::factory(200)->create();
    }
}
