<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Reply;
use App\Models\comment;
use App\Http\Resources\ReplyResource;

use Illuminate\Http\Request;

class ReplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Comment $comment)
    {
        $replys = Reply::whereComment_id($comment->id)->orderBy('updated_at', 'desc')->get();
        // $collection = $replys->getCollection();
        return ReplyResource::collection($replys);
    }


    public function store(Request $request, Comment $comment)
    {
        $user = auth('api')->user();

        Reply::create([

            'content'  => request()->content,
            'user_id'  => $user->id,
            'comment_id'  => $comment->id,

        ]);
        return response()->json('This Reply Is Add To Your House');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function show(Reply $reply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function edit(Reply $reply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reply $reply)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reply  $reply
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reply $reply)
    {
        //
    }
}
