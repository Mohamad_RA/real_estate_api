<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HomeHouseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            'id'            => $this->id,
            'description'   => $this->description,
            'street_address' => $this->street_address,
            'price'         => $this->price,
            'base_image'    => $this->base_image,
            'other_image'   => json_decode($this->other_image, TRUE),
            'bedrooms'      => $this->bedrooms,
            'bathrooms'     => $this->bathrooms,
            'sqft_living'   => $this->sqft_living,
            'sqft_lot'      => $this->sqft_lot,
            'floors'        => $this->floors,
            'waterfront'    => $this->waterfront,
            'view'          => $this->view,
            'condition'     => $this->condition,
            'grade'         => $this->grade,
            'sqft_above'    => $this->sqft_above,
            'sqft_basement' => $this->sqft_basement,
            'yr_built'      => $this->yr_built,
            'yr_renovated'  => $this->yr_renovated,
            'zipcode'       => $this->zipcode,
            'lattitude'     => $this->lattitude,
            'longitude'     => $this->longitude,
            'isliked'       => $this->isLiked(),
            'countlikes'    => $this->userslikes()->count(),
            'countcomments' => $this->userscomments()->count(),
            'user'          => $this->user
        ];
    }

    public function isLiked()
    {

        $user = auth('api')->user();

        if (is_null($user)) {
            return false;
        } else {
            if (is_null($user->houseslikes->find($this->id))) {
                return false;
            } else {
                return true;
            }
        }
    }
}
