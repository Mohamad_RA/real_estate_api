<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Owner extends Model
{
    use HasFactory;
    protected $fillable = ["name"];
    public $timestamp = false;


    public function houses()
    {
        return $this->belongsToMany('App\Models\House', 'house_owners', 'owner_id', 'house_id');
    }
}
